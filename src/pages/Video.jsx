import {
  ThumbDownAlt,
  ThumbDownAltOutlined,
  ThumbUpAlt,
  ThumbUpAltOutlined,
} from "@mui/icons-material";

import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import { fetchSuccess, like, dislike } from "../redux/videoSlice";
import TimeAgo from "react-timeago";
import { subscription } from "../redux/userSlice";
import Recommendation from "components/Recommendation";
import { useValue } from "context/NotifiContext";
import axiosClient from "api/axiosClient";

const Container = styled.div`
  display: flex;
  gap: 24px;
`;

const Content = styled.div`
  flex: 5;
`;

const VideoWrapper = styled.div``;

const Title = styled.h1`
  font-size: 16px;
  font-weight: 400;
  margin: 20px 0 10px 0;
  color: white;
`;

const Details = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Info = styled.span`
  color: white;
`;

const Buttons = styled.div`
  display: flex;
  gap: 20px;
  color: white;
`;
const Button = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
  cursor: pointer;
`;

const Hr = styled.hr`
  margin: 15px 0;
  border: 0.5px solid white;
`;

const Channel = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Img = styled.img`
  width: 50px;
  height: 50px;
  border-radius: 50%;
`;

const ChannelInfo = styled.div`
  display: flex;
  gap: 20px;
`;

const ChannelDetails = styled.div`
  display: flex;
  flex-direction: column;
  color: white;
`;

const ChannelName = styled.span`
  font-weight: 500;
`;

const ChannelCounters = styled.span`
  margin: 5px 0 20px 0;
  color: white;
  font-size: 12px;
`;

const Description = styled.p`
  font-size: 14px;
`;

const Subcribe = styled.button`
  color: white;
  background-color: #cc1a00;
  font-weight: 500;
  border: none;
  border-radius: 3px;
  height: max-content;
  padding: 10px 20px;
  cursor: pointer;
`;

export const Video = () => {
  const { currentUser, token } = useSelector((state) => state.user);
  const { currentVideo } = useSelector((state) => state.video);
  const { dispatchNotifi } = useValue();

  const dispatch = useDispatch();
  const path = useLocation().pathname.split("/")[2];

  const [channel, setChannel] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      try {
        const options = {
          method: "get",
          headers: {
            "content-type": "application/vnd.api+json",
            "Access-Token": token,
          },
          url: `/videos?include=createdBy&filter[id]=${path}`,
        };
        const videoRes = await axiosClient(options);

        const videoData = videoRes.data.data[0];

        setChannel(videoRes.data.included[0]);
        dispatch(fetchSuccess(videoData));
      } catch (e) {
        dispatchNotifi({
          type: "UPDATE_ALERT",
          payload: {
            open: true,
            severity: "error",
            message: e?.response?.statusText,
          },
        });
      }
    };

    fetchData();
  }, [path]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    const increaseViews = async () => {
      try {
        const url = `/videos/${currentVideo.id}`;
        const options = {
          method: "put",
          headers: {
            "content-type": "application/vnd.api+json",
            "Access-Token": token,
          },
          data: {
            data: {
              id: currentVideo.id,
              type: "videos",
              attributes: {
                views: parseInt(currentVideo?.attributes?.views) + 1,
              },
            },
          },
          url,
        };
        const res = await axiosClient(options);
        dispatch(fetchSuccess(res.data.data));
      } catch (e) {
        dispatchNotifi({
          type: "UPDATE_ALERT",
          payload: {
            open: true,
            severity: "error",
            message: e?.response?.statusText,
          },
        });
      }
    };

    if (currentUser?.id) increaseViews();
  }, [currentUser]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleLike = async () => {
    if (currentUser === null) return alertLogin();

    const url = `/users/${currentVideo.id}/like`;
    const options = {
      method: "put",
      headers: {
        "content-type": "application/vnd.api+json",
        "Access-Token": token,
      },
      url,
    };
    try {
      await axiosClient(options);
    } catch (e) {
      dispatchNotifi({
        type: "UPDATE_ALERT",
        payload: {
          open: true,
          severity: "error",
          message: e?.response?.statusText,
        },
      });
    }
    dispatch(like(currentUser?.id));
  };

  const handleDislike = async () => {
    if (currentUser === null) return alertLogin();

    const url = `/users/${currentVideo.id}/dislike`;
    const options = {
      method: "put",
      headers: {
        "content-type": "application/vnd.api+json",
        "Access-Token": token,
      },
      url,
    };
    try {
      await axiosClient(options);
    } catch (e) {
      dispatchNotifi({
        type: "UPDATE_ALERT",
        payload: {
          open: true,
          severity: "error",
          message: e?.response?.statusText,
        },
      });
    }
    dispatch(dislike(currentUser.id));
  };

  const handleSub = async () => {
    if (currentUser === null) return alertLogin();

    const url = currentUser?.attributes?.subscribedUsers?.includes(channel.id)
      ? `/users/${channel.id}/unsubscribe`
      : `/users/${channel.id}/subscribe`;
    const options = {
      method: "put",
      headers: {
        "content-type": "application/vnd.api+json",
        "Access-Token": token,
      },
      url,
    };
    try {
      await axiosClient(options);
    } catch (e) {
      dispatchNotifi({
        type: "UPDATE_ALERT",
        payload: {
          open: true,
          severity: "error",
          message: e?.response?.statusText,
        },
      });
    }
    dispatch(subscription(channel.id));
  };

  const alertLogin = () => {
    dispatchNotifi({
      type: "UPDATE_ALERT",
      payload: {
        open: true,
        severity: "error",
        message: "Need to login",
      },
    });
  };

  return (
    <Container>
      <Content>
        <VideoWrapper>
          <iframe
            width="100%"
            height="720"
            src={currentVideo?.attributes?.videoUrl}
            title={currentVideo?.attributes?.title}
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowfullscreeneen="true"
          ></iframe>
        </VideoWrapper>
        <Title>{currentVideo?.attributes?.title}</Title>
        <Details>
          <Info>
            {currentVideo?.attributes?.views} Views *{" "}
            <TimeAgo date={currentVideo?.attributes?.createdAt} />
          </Info>
          <Buttons>
            <Button onClick={handleLike}>
              {currentVideo?.attributes?.likes.includes(currentUser?.id) ? (
                <ThumbUpAlt />
              ) : (
                <ThumbUpAltOutlined />
              )}{" "}
              {currentVideo?.attributes?.likes?.length}
            </Button>
            <Button onClick={handleDislike}>
              {currentVideo?.attributes?.dislikes.includes(currentUser?.id) ? (
                <ThumbDownAlt />
              ) : (
                <ThumbDownAltOutlined />
              )}{" "}
              {currentVideo?.attributes?.dislikes?.length}
            </Button>
          </Buttons>
        </Details>
        <Hr />

        <Channel>
          <ChannelInfo>
            <Img src={channel?.attributes?.img} />
            <ChannelDetails>
              <ChannelName>{channel?.attributes?.fullName}</ChannelName>
              <ChannelCounters>
                {channel?.attributes?.subscribers} subscribers
              </ChannelCounters>
              <Description>{currentVideo?.attributes?.desc}</Description>
            </ChannelDetails>
          </ChannelInfo>
          <Subcribe onClick={handleSub}>
            {currentUser?.attributes?.subscribedUsers?.includes(channel?.id)
              ? "Subscribed"
              : "Subscribe"}
          </Subcribe>
        </Channel>
      </Content>
      <Recommendation />
    </Container>
  );
};
