import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import {
  loginFailure,
  loginStart,
  loginSuccess,
  getInfoSuccess,
  logout,
} from "../redux/userSlice";
import { useNavigate } from "react-router-dom";
import { useValue } from "context/NotifiContext";
import axiosClient from "api/axiosClient";

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 56px);
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: #202020;
  border: 1px solid #202020;
  padding: 20px 50px;
  gap: 10px;
`;

const Title = styled.h1`
  font-size: 24px;
`;

const SubTitle = styled.h2`
  font-size: 20px;
  font-weight: 300;
`;

const Input = styled.input`
  border: 1px solid #373737;
  border-radius: 3px;
  padding: 10px;
  background-color: transparent;
  width: 100%;
  color: white;
`;

const Button = styled.button`
  border-radius: 3px;
  border: none;
  padding: 10px 20px;
  font-weight: 500;
  background-color: #373737;
  color: white;
  cursor: pointer;
`;

const More = styled.div`
  display: flex;
  margin-top: 10px;
  font-size: 12px;
  color: white;
`;

const Links = styled.div`
  margin-left: 50px;
`;

const Link = styled.span`
  margin-left: 30px;
`;

const Signin = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirm, setPasswordConfirm] = useState("");
  const [fullName, setFullName] = useState("");
  const { dispatchNotifi } = useValue();
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    dispatch(loginStart());
    try {
      const url = "/auth/signin";
      const options = {
        method: "post",
        headers: {
          "content-type": "application/vnd.api+json",
        },
        data: {
          data: {
            attributes: {
              username: name,
              password: password,
            },
          },
        },
        url,
      };
      const res = await axiosClient(options);
      dispatch(loginSuccess(res.data.token));
      await getInfo(res.data.token);
    } catch (e) {
      dispatch(loginFailure());
      dispatchNotifi({
        type: "UPDATE_ALERT",
        payload: {
          open: true,
          severity: "error",
          message: e?.response?.statusText,
        },
      });
    }
  };

  const getInfo = async (token) => {
    try {
      const url = "/auth/me";
      const options = {
        method: "get",
        headers: {
          "content-type": "application/vnd.api+json",
          "Access-Token": token,
        },
        url,
      };
      const res = await axiosClient(options);
      dispatch(getInfoSuccess(res.data.data));
      navigate("/");
    } catch (e) {
      dispatch(loginFailure());
      dispatchNotifi({
        type: "UPDATE_ALERT",
        payload: {
          open: true,
          severity: "error",
          message: e?.response?.statusText,
        },
      });
    }
  };

  const handleSignup = async (e) => {
    e.preventDefault();
    dispatch(loginStart());
    const url = "/users";
    const options = {
      method: "post",
      headers: {
        "content-type": "application/vnd.api+json",
      },
      data: {
        data: {
          type: "users",
          attributes: {
            username: name,
            fullName: fullName,
            email: email,
            password: password,
            passwordConfirmation: passwordConfirm,
          },
        },
      },
      url,
    };
    try {
      const res = await axiosClient(options);
      if (res.status === 201) {
        dispatchNotifi({
          type: "UPDATE_ALERT",
          payload: {
            open: true,
            severity: "success",
            message: "Registed",
          },
        });
      }
    } catch (e) {
      dispatch(loginFailure());
      dispatchNotifi({
        type: "UPDATE_ALERT",
        payload: {
          open: true,
          severity: "error",
          message: e?.response?.statusText,
        },
      });
    }
  };

  useEffect(() => {
    const clearSession = () => {
      dispatch(logout());
    };

    clearSession();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container>
      <Wrapper>
        <Title>Sign in</Title>
        <SubTitle>to continue to App</SubTitle>
        <Input
          placeholder="username"
          onChange={(e) => setName(e.target.value)}
        ></Input>
        <Input
          type="password"
          placeholder="password"
          onChange={(e) => setPassword(e.target.value)}
        ></Input>
        <Button onClick={handleLogin}>Sign in</Button>
        <Title>Or</Title>
        <Input
          placeholder="username"
          onChange={(e) => setName(e.target.value)}
        ></Input>
        <Input
          placeholder="full Name"
          onChange={(e) => setFullName(e.target.value)}
        ></Input>
        <Input
          placeholder="email"
          onChange={(e) => setEmail(e.target.value)}
        ></Input>
        <Input
          type="password"
          placeholder="password"
          onChange={(e) => setPassword(e.target.value)}
        ></Input>
        <Input
          type="password"
          placeholder="password Confirm"
          onChange={(e) => setPasswordConfirm(e.target.value)}
        ></Input>
        <Button onClick={handleSignup}>Sign up</Button>
      </Wrapper>
      <More>
        English
        <Links>
          <Link>Help</Link>
          <Link>Privacy</Link>
        </Links>
      </More>
    </Container>
  );
};

export default Signin;
