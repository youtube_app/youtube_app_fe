import Card from "components/Card";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useSelector } from "react-redux";
import { useValue } from "context/NotifiContext";
import axiosClient from "api/axiosClient";

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export const Home = ({ type, textSearch }) => {
  const { token } = useSelector((state) => state.user);
  const [videos, setVideo] = useState([]);
  const { dispatchNotifi } = useValue();

  useEffect(() => {
    const fetchVideos = async () => {
      try {
        const url = `/videos`;
        let params = {
          filter: {
            type: type,
          },
        };
        if (textSearch !== "" && textSearch !== null) {
          params = {
            filter: {
              ...params["filter"],
              search: textSearch,
            },
          };
        }
        const options = {
          method: "get",
          headers: {
            "content-type": "application/vnd.api+json",
            "Access-Token": token,
          },
          params: params,
          url,
        };
        const res = await axiosClient(options);
        setVideo(res.data.data || []);
      } catch (e) {
        setVideo([]);
        dispatchNotifi({
          type: "UPDATE_ALERT",
          payload: {
            open: true,
            severity: "error",
            message: e?.response?.statusText,
          },
        });
      }
    };

    fetchVideos();
  }, [type, textSearch]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container>
      {videos?.map((video) => (
        <Card key={video.id} video={video}></Card>
      ))}
    </Container>
  );
};
