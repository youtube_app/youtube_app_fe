const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN_SUCCESSED":
      return { ...state, currentUser: action.payload };
    case "LOGIN_FAILED":
      return { ...state, alert: action.payload };
    case "IMPORT_FAILED":
      return { ...state, alert: action.payload };
    case "IMPORT_SUCCESSED":
      return { ...state, alert: action.payload };
    case "DELETE_SUCCESSED":
      return { ...state, alert: action.payload };
    case "NEXT_STEP_FAILED":
      return { ...state, alert: action.payload };
    case "START_LOADING":
      return { ...state, loading: true };
    case "END_LOADING":
      return { ...state, loading: false };
    case "UPDATE_ALERT":
      return { ...state, alert: action.payload };
    default:
      throw new Error("No matched action!");
  }
};

export default reducer;
