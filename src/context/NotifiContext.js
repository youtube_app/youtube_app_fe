import { createContext, useContext, useReducer } from "react";
import reducer from "./NotifiReducer";

const initialState = {
  currentUser: {},
  openLogin: false,
  loading: false,
  alert: { open: false, severity: "info", message: "" },
};

const Context = createContext(initialState);

export const useValue = () => {
  return useContext(Context);
};

const NotifiContext = ({ children }) => {
  const [state, dispatchNotifi] = useReducer(reducer, initialState);
  return (
    <Context.Provider value={{ state, dispatchNotifi }}>
      {children}
    </Context.Provider>
  );
};

export default NotifiContext;
