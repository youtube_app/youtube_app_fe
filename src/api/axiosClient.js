import axios from "axios";
import { deserialize } from "deserialize-json-api";
import url from "../../src/config/url";

const axiosClient = axios.create({
  baseURL: url.API_URL,
});

// Interceptors (lafm cho tat ca request/ response)
// Add a request interceptor
axiosClient.interceptors.request.use(
  function (config) {
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosClient.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    const contentType = response.headers.getContentType();

    if (contentType && contentType.indexOf("json") !== -1) {
      return deserialize(response);
    } else {
      return response;
    }
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    error.response.errors = error.response?.data?.errors || [
      { detail: error.response?.statusText },
    ];
    const current_path = window.location.href.split("/").pop();
    // if (error.response?.status === 401 && current_path !== "login") {
    //   return (window.location = "/login");
    // } else if (error.response?.status === 401) {
    //   return error.response;
    // }
    // if (error.response?.status === 404 && current_path !== "not_found") {
    //   return (window.location = "/not_found");
    // }
    return deserialize(error.response);
  }
);

export default axiosClient;
