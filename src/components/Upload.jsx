import React, { useState } from "react";
import styled from "styled-components";
import getYouTubeID from "get-youtube-id";
import YouTube from "react-youtube";

import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useValue } from "context/NotifiContext";
import axiosClient from "api/axiosClient";

const Container = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: #000000a7;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 99;
`;
const Wrapper = styled.div`
  width: 600px;
  height: 600px;
  background-color: #202020;
  color: white;
  padding: 20px;
  display: flex;
  flex-direction: column;
  gap: 20px;
  position: relative;
`;
const Close = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
  cursor: pointer;
`;
const Title = styled.h1`
  text-align: center;
`;

const Input = styled.input`
  border: 1px solid #999;
  color: white;
  border-radius: 3px;
  padding: 10px;
  background-color: transparent;
  z-index: 999;
`;

const Button = styled.button`
  border-radius: 3px;
  border: none;
  padding: 10px 20px;
  font-weight: 500;
  cursor: pointer;
  background-color: #999;
  color: white;
`;

const Desc = styled.textarea`
  border: 1px solid #999;
  color: white;
  border-radius: 3px;
  padding: 10px;
  background-color: transparent;
  min-height: 50px;
`;

const Upload = ({ setOpen }) => {
  const [id, setId] = useState("");
  const [inputs, setInputs] = useState({});
  const { token } = useSelector((state) => state.user);
  const { dispatchNotifi } = useValue();

  const navigate = useNavigate();

  const opts = {
    height: "390",
    width: "640",
    playerVars: {
      autoplay: 1,
    },
  };

  const handleChangeUrl = (e) => {
    setId(getYouTubeID(e.target.value));
  };

  const handleChange = (e) => {
    setInputs((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  const onPlayerReady = (event) => {
    // access to player in all event handlers via event.target
    event.target.pauseVideo();
  };

  const handleShareVideo = async () => {
    try {
      const url = "/videos";
      const options = {
        method: "post",
        headers: {
          "content-type": "application/vnd.api+json",
          "Access-Token": token,
        },
        data: {
          data: {
            type: "videos",
            attributes: {
              title: inputs.title,
              desc: inputs.desc,
              imgUrl: `http://img.youtube.com/vi/${id}/0.jpg`,
              videoUrl: `https://www.youtube.com/embed/${id}`,
              tags: [],
            },
          },
        },
        url,
      };
      const res = await axiosClient(options);
      setOpen(false);
      res.status === 201 && navigate(`/video/${res.data.data.id}`);
    } catch (e) {
      dispatchNotifi({
        type: "UPDATE_ALERT",
        payload: {
          open: true,
          severity: "error",
          message: e?.response?.statusText,
        },
      });
    }
  };
  return (
    <Container>
      <Wrapper>
        <Close onClick={() => setOpen(false)}>X</Close>
        <Title>Upload a New Video</Title>
        <Input
          name="url"
          placeholder="Youtube Url"
          onChange={handleChangeUrl}
        />
        <Input
          type="text"
          placeholder="Title"
          name="title"
          onChange={handleChange}
        />
        <Desc
          placeholder="Description"
          name="desc"
          rows={8}
          onChange={handleChange}
        />
        <Button onClick={handleShareVideo}>Share Video</Button>
        {id && <YouTube videoId={id} opts={opts} onReady={onPlayerReady} />}
      </Wrapper>
    </Container>
  );
};

export default Upload;
