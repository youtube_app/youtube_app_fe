import React, { useEffect, useState } from "react";
import styled from "styled-components";
import Card from "./Card";
import { useSelector } from "react-redux";
import axiosClient from "api/axiosClient";

const Container = styled.div`
  flex: 2;
`;

const Recommendation = ({ tags }) => {
  const { token } = useSelector((state) => state.user);
  const [videos, setVideos] = useState([]);

  useEffect(() => {
    const fetchVideos = async () => {
      const url = `/videos`;
      const options = {
        method: "get",
        headers: {
          "content-type": "application/vnd.api+json",
          "Access-Token": token,
        },
        url,
      };
      const res = await axiosClient(options);

      setVideos(res.data.data);
    };
    fetchVideos();
  }, []);

  return (
    <Container>
      {videos.map((video) => (
        <Card type="sm" key={video.id} video={video} />
      ))}
    </Container>
  );
};

export default Recommendation;
