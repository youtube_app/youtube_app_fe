import { Alert, Snackbar } from "@mui/material";
import React from "react";
import { useValue } from "context/NotifiContext";

const Notification = () => {
  const {
    state: { alert },
    dispatchNotifi,
  } = useValue();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") return;
    dispatchNotifi({
      type: "UPDATE_ALERT",
      payload: { ...alert, open: false },
    });
  };

  return (
    <Snackbar
      open={alert.open}
      autoHideDuration={5000}
      onClose={handleClose}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
    >
      <Alert
        onClose={handleClose}
        severity={alert.severity}
        sx={{ width: "100%" }}
        variant="filled"
        elevation={6}
      >
        {alert.message}
      </Alert>
    </Snackbar>
  );
};

export default Notification;
