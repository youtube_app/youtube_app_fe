import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import TimeAgo from "react-timeago";
import { useSelector } from "react-redux";
import axiosClient from "api/axiosClient";

const Container = styled.div`
  width: ${(props) => props.type !== "sm" && "360px"};
  margin-bottom: ${(props) => (props.type === "sm" ? "10px" : "45px")};
  cursor: pointer;
  display: ${(props) => props.type === "sm" && "flex"};
  gap: 10px;
`;

const Img = styled.img`
  width: 100%;
  height: ${(props) => (props.type === "sm" ? "120px" : "200px")};
  background-color: #999;
  flex: 1;
`;

const Detail = styled.div`
  display: flex;
  margin-top: ${(props) => props.type !== "sm" && "16px"};
  gap: 12px;
  flex: 1;
`;

const ChannelImg = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  background-color: #999;
  display: ${(props) => props.type === "sm" && "none"};
`;

const Texts = styled.div``;

const Title = styled.h1`
  font-size: 16px;
  font-weight: 500;
  color: white;
`;

const ChannelName = styled.h2`
  font-size: 14px;
  color: white;
  margin: 10px 0;
`;

const Info = styled.div`
  font-size: 14px;
  color: white;
`;

const Card = ({ type, video }) => {
  const { token } = useSelector((state) => state.user);
  const [channel, setChannel] = useState({});
  const videoAttributes = video?.attributes;

  useEffect(() => {
    const fetchChannel = async () => {
      const options = {
        method: "get",
        headers: {
          "content-type": "application/vnd.api+json",
          "Access-Token": token,
        },
        data: {
          filter: {
            id: videoAttributes?.createdById,
          },
        },
        url: `/users?filter[id]=${videoAttributes?.createdById}`,
      };
      const res = await axiosClient(options);
      setChannel(res.data.data[0]);
    };
    fetchChannel();
  }, [videoAttributes]);

  return (
    <Link to={`/video/${video?.id}`} style={{ textDecoration: "none" }}>
      <Container type={type}>
        <Img src={videoAttributes?.imgUrl} type={type} />
        <Detail type={type}>
          <ChannelImg type={type} src={channel?.attributes?.img} />
          <Texts>
            <Title>{videoAttributes?.title}</Title>
            <ChannelName>{channel?.attributes?.fullName}</ChannelName>
            <Info>
              {videoAttributes?.views} Views *{" "}
              <TimeAgo date={videoAttributes?.createdAt} />
            </Info>
          </Texts>
        </Detail>
      </Container>
    </Link>
  );
};

export default Card;
