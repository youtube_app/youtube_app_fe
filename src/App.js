import styled from "styled-components";
import { Menu } from "./components/Menu";
import { Navbar } from "./components/Navbar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Video } from "pages/Video";
import { Home } from "pages/Home";
import Signin from "pages/Signin";
import NotifiContext from "context/NotifiContext";
import Notification from "components/Notification";
import ActionCable from "actioncable";
import ActionCableProvider from "react-actioncable-provider";
import { useSelector } from "react-redux";
import { useState } from "react";

const Container = styled.div`
  display: flex;
`;

const Main = styled.div`
  flex: 7;
  background-color: #181818;
  color: white;
`;
const Wrapper = styled.div`
  padding: 22px 96px;
`;

function App() {
  const { currentUser } = useSelector((state) => state.user);
  const [textSearch, setTextSearch] = useState("");
  // const cable = ActionCable.createConsumer("ws://localhost:3000/cable");

  const cable = ActionCable.createConsumer(
    "wss://young-ridge-20338-1df48cdefaf3.herokuapp.com/cable"
  );
  try {
    cable.subscriptions.create("MessagesChannel", {
      received(data) {
        if (
          currentUser?.attributes.subscribedUsers.includes(data?.created_by_id)
        ) {
          alert(data.content);
        }
      },
    });
  } catch (error) {
    console.log(error);
  }

  return (
    <Container>
      <BrowserRouter>
        {/* menu */}
        <Menu />
        <ActionCableProvider cable={cable}>
          {/* main */}
          <Main>
            <NotifiContext>
              <Notification />

              <Navbar setTextSearch={setTextSearch}></Navbar>
              <Wrapper>
                <Routes>
                  <Route path="/">
                    <Route
                      index
                      element={<Home type="random" textSearch={textSearch} />}
                    />
                    <Route
                      path="trends"
                      element={<Home type="trend" textSearch={textSearch} />}
                    />
                    <Route
                      path="subscriptions"
                      element={<Home type="sub" textSearch={textSearch} />}
                    />
                    <Route
                      path="libraries"
                      element={<Home type="lib" textSearch={textSearch} />}
                    />
                    <Route path="signin" element={<Signin />} />
                    <Route path="signout" element={<Signin />} />
                    <Route path="video">
                      <Route path=":id" element={<Video />} />
                    </Route>
                  </Route>
                </Routes>
              </Wrapper>
            </NotifiContext>
          </Main>
        </ActionCableProvider>
      </BrowserRouter>
    </Container>
  );
}

export default App;
