const { REACT_APP_ENVIRONMENT, API_URL } = process.env;

const ENVIRONMENT = {
  DEVELOPMENT: "development",
  STAGING: "staging",
  DEMO: "demo",
  PRODUCTION: "production",
};

const getAPIUrl = () => {
  if (API_URL) {
    return API_URL;
  }
  switch (REACT_APP_ENVIRONMENT) {
    case ENVIRONMENT.DEVELOPMENT:
      return "http://localhost:3000/v1";
    case ENVIRONMENT.PRODUCTION:
      return "https://young-ridge-20338-1df48cdefaf3.herokuapp.com/v1";
    default:
      return "http://localhost:3000/v1";
  }
};

// eslint-disable-next-line
export default {
  API_URL: getAPIUrl(),
};
