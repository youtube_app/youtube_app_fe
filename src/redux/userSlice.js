import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  token: null,
  currentUser: null,
  loading: false,
  error: false,
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    loginStart: (state) => {
      state.loading = true;
    },
    loginSuccess: (state, action) => {
      state.token = action.payload;
      state.error = false;
    },
    loginFailure: (state) => {
      state.token = null;
      state.currentUser = null;
      state.loading = false;
      state.error = true;
    },
    logout: (state) => {
      return initialState;
    },
    getInfoSuccess: (state, action) => {
      state.loading = false;
      state.currentUser = action.payload;
      state.error = false;
    },
    subscription: (state, action) => {
      if (
        state.currentUser.attributes.subscribedUsers.includes(action.payload)
      ) {
        state.currentUser.attributes.subscribedUsers.splice(
          state.currentUser.attributes.subscribedUsers.findIndex(
            (channelId) => channelId === action.payload
          ),
          1
        );
      } else {
        state.currentUser.attributes.subscribedUsers.push(action.payload);
      }
    },
  },
});

export const {
  loginStart,
  loginSuccess,
  loginFailure,
  logout,
  getInfoSuccess,
  subscription,
} = userSlice.actions;

export default userSlice.reducer;
