import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  currentVideo: null,
  loading: false,
  error: false,
};

export const videoSlice = createSlice({
  name: "video",
  initialState,
  reducers: {
    fetchStart: (state) => {
      state.loading = true;
    },
    fetchSuccess: (state, action) => {
      state.loading = false;
      state.currentVideo = action.payload;
      state.error = false;
    },
    fetchFailure: (state) => {
      state.currentVideo = null;
      state.loading = false;
      state.error = true;
    },
    like: (state, action) => {
      if (!state.currentVideo.attributes.likes.includes(action.payload)) {
        state.currentVideo.attributes.likes.push(action.payload);
        state.currentVideo.attributes.dislikes.splice(
          state.currentVideo.attributes.dislikes.findIndex(
            (createdById) => createdById === action.payload
          ),
          1
        );
      } else {
        state.currentVideo.attributes.likes.splice(
          state.currentVideo.attributes.likes.findIndex(
            (createdById) => createdById === action.payload
          ),
          1
        );
      }
    },
    dislike: (state, action) => {
      if (!state.currentVideo.attributes.dislikes.includes(action.payload)) {
        state.currentVideo.attributes.dislikes.push(action.payload);
        state.currentVideo.attributes.likes.splice(
          state.currentVideo.attributes.likes.findIndex(
            (createdById) => createdById === action.payload
          ),
          1
        );
      } else {
        state.currentVideo.attributes.dislikes.splice(
          state.currentVideo.attributes.dislikes.findIndex(
            (createdById) => createdById === action.payload
          ),
          1
        );
      }
    },
  },
});

export const { fetchStart, fetchSuccess, fetchFailure, like, dislike } =
  videoSlice.actions;

export default videoSlice.reducer;
